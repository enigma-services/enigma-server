# Enigma Server

Server executable for Mpic sealed chip USB encryption device.

![ALT](/images/enigmakey.png)

To protect personal integrity such as saved data and secure communication, and when transferring sensitive information into the “Data Cloud”, “DropBox” etc., a secure, uncompromised  Encoding/Decoding device is needed. The Enigma Key is the  real answer.

There are many “similar looking” systems on the market claiming they can be used for this purpose. In almost all cases, the systems are “software oriented”. It means that the encoding and decoding process is done by the host using some programming strategies. (mathematical algorithms e.g. AES…). In the case of the ENK, all of the processes and databases needed for encryption are encapsulated in the ENK which is securely “code and write protected”.

The software support running in the host supports only the data transfer to and from the ENK via the USB channel, using a high security communications protocoll, and is not involved in the Encoding/Decoding processes.

![ALT](/images/enigma_2000logic-300x206.jpg)

The ENK encryption process is built around the OTP (One-Time-Pad) encoding/decoding scheme using  EHT’s (EnigmaHashTables)  , which are created in the ENK by the user, when needed, or can be downloaded in encoded form by the user when received from a sender.

If you want more information about the ENK philosophy, available functions, results of testing, producer and distributor networks strategy and any other subjects can, however, only be passed to you in encrypted format. This is of course the case for all communication where details of the ENK is involved. For this you need an operational ENK .

Software that needs to be installed before the ENK can be used  you will find on the Installation page in this document.

When you have downloaded and installed the program, the Server is requesting a licence data file which has to be downloaded into the ENK’s APDB (Application DataBase). Just send the request online and the license data will be emailed to you. More detailed information on the Installation Page.

The DataVault is just one application where basic encoding and decoding of users data is handled.

Because the EHT  is downloaded in both the Sender’s  and Receiver’s ENK, communication with encoding decoding can be started.